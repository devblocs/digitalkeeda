# DgitalKeeda
Social Media Marketing Agency

## Project Description
Digital keeda is an imaginary digital marketing company focus on SEO, SMM and Website development, the project was about developing a portfolio for the company. Key features of this website are to create a portfolio for the company, use appropriate content and keywords, develop this website responsive for all sizes of screens.

## Tech Specifications
- **Front-end:**
    - HTML5 & CSS3
    - JavaScript

### Future updates
- Converting static website to dynamic website
- Use server side scripting language
- Creation of admin panel for admins
- Possibility of of converting to wordpress theme
